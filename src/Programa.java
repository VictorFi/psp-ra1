
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Programa implements ActionListener,Observer {
    private JTextField tfEnlace;
    private JButton bDescargar;
    private JPanel pDescargas;
    private JTextField tfRuta;
    private JButton bCambiarRuta;
    private JPanel panel;
    private JButton bRegistro;

    private Descarga descarga;
    private ArrayList<Descarga> listaDescargas;

    private File rutaFichero;


    public Programa() {
        JFrame frame = new JFrame("Programa");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,500);
        frame.setLocation(500,100);
        inicializar();
        frame.pack();
        frame.setVisible(true);

        listaDescargas = new ArrayList<>();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando) {
            case "Descargar":
                descarga = new Descarga(tfEnlace.getText(), tfRuta.getText());
                descarga.addObserver(this);
                listaDescargas.add(descarga);
                listarDescargas();
                break;
            case "Cambiar":
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int resp = fileChooser.showOpenDialog(null);
                if (resp == JFileChooser.APPROVE_OPTION){
                    rutaFichero = fileChooser.getSelectedFile();
                }else{
                    break;
                }

                tfRuta.setText(rutaFichero.toString());
                break;
            case "Registro":
                Registro registro = new Registro();
                break;
        }
    }

    public void inicializar() {
        tfRuta.setText("C:\\Users\\" + System.getProperty("user.name") + "\\Desktop");
        pDescargas.setLayout(new BoxLayout(pDescargas, BoxLayout.Y_AXIS));
        bDescargar.addActionListener(this);
        bCambiarRuta.addActionListener(this);
        bRegistro.addActionListener(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        String comando = (String) arg;

        switch (comando) {
            case "eliminar_descarga":
                   Descarga d = (Descarga) o;
                    listaDescargas.remove(d);
                    listarDescargas();
                break;

        }
    }
    private void listarDescargas(){
        pDescargas.removeAll();
        if(listaDescargas.size() >= 1){
            for(Descarga d:listaDescargas){
                pDescargas.add(d.pDescarga);
            }
        }
        pDescargas.revalidate();
    }
}
