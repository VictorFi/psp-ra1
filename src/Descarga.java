

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;

public class Descarga extends Observable implements ActionListener{
    private JLabel lTxtEnlace;
    private JProgressBar progressBar1;
    private JLabel lTotal;
    private JButton bCancelar;
    private JButton bEliminar;
    public JPanel pDescarga;
    private JLabel lTamano;
    private JLabel lTamanoF;

    private TareaDescarga tareaDescarga;
    private Observer observador;

    public static int index = 0;
    public int indexAux = index;
    private String enlace;

     private final static Logger logger = LogManager.getLogger(Descarga.class);

    public Descarga(String enlace,String ruta){
        this.enlace = enlace;
        //PropertyConfigurator.configure("log4j2.xml");
        index++;
        lTxtEnlace.setText(enlace);
        bCancelar.addActionListener(this);
        bEliminar.addActionListener(this);
        tareaDescarga = new TareaDescarga(enlace,ruta);
        tareaDescarga.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals("tamanoFichero")){
                    lTamanoF.setText("/"+evt.getNewValue().toString()+"kb");
                }else if(evt.getPropertyName().equals("descarga")){
                    lTamano.setText(evt.getNewValue().toString()+"kb");
                }else if(evt.getPropertyName().equals("progress")){
                    progressBar1.setValue((Integer) evt.getNewValue());
                }else if(evt.getPropertyName().equals("porcentaje")){
                    lTotal.setText( evt.getNewValue()+"% de 100%");
                    if(evt.getNewValue().equals(100)){
                        logger.info(" - Enlace: "+enlace+" [Descarga Completada]");
                    }
                }else if(evt.getPropertyName().equals("descarga_cancelada")){
                    notifyObservers();
                }
            }
        });
        tareaDescarga.execute();

    }

    @Override
    public void addObserver(Observer observador) {
        this.observador = observador;
    }

    @Override
    public void notifyObservers() {
        if(observador != null){
            observador.update(this,"descarga_cancelada");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Cancelar":
                if(tareaDescarga != null){
                    tareaDescarga.cancel(false);
                    logger.info(" - Enlace: "+enlace+" [Descarga Cancelada]");
                }
                break;
            case "Eliminar":
                    observador.update(this,"eliminar_descarga");
                break;

        }
    }
}
