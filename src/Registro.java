import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class Registro extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextArea taRegistro;

    public Registro() {
        pack();
        setVisible(true);
        setSize(500,300);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        try {
            cargarRegistro();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void cargarRegistro() throws FileNotFoundException {
        FileReader file = new FileReader("GestorDescargas.log");
        BufferedReader bufferedReader = new BufferedReader(file);
        String cadena;

        try {
            while((cadena = bufferedReader.readLine()) != null){
                taRegistro.append(cadena+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
