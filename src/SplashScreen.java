import javax.swing.*;

import java.net.URL;

import static com.sun.deploy.uitoolkit.ToolkitStore.dispose;

public class SplashScreen{


    private JPanel panel1;
    private JLabel lImagen;
    JFrame frame;

    public SplashScreen(){
        frame = new JFrame("SplashScreen");
        frame.setUndecorated(true);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(400,400);
        frame.setLocation(500,100);
        frame.setResizable(false);
        frame.setVisible(true);
    }
    public void splash() throws Exception {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.dispose();
    }
}
