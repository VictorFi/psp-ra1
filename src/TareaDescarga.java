import javax.swing.*;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class TareaDescarga extends SwingWorker<Void,Void> {

    private String enlace;
    private String ruta;
    private boolean cancelada;


    public TareaDescarga(String enlace, String ruta) {
        this.enlace = enlace;
        this.ruta = ruta + (enlace.substring(enlace.lastIndexOf("/")));
    }

    @Override
    protected Void doInBackground() throws Exception {
        URL url = new URL(enlace);
        URLConnection conexion = url.openConnection();

        if (isCancelled()) {

        } else {

            int tamanoFichero = conexion.getContentLength();

            firePropertyChange("tamanoFichero", 0, tamanoFichero);

            InputStream is = url.openStream();
            FileOutputStream fos = new FileOutputStream(ruta);

            byte[] bytes = new byte[8192];
            int longitud = 0;
            int progresoDescarga = 0;
            long tiempoDescarga = 0;
            long longitudAcumulada = 0;
            tiempoDescarga = System.currentTimeMillis();
            while ((longitud = is.read(bytes)) != -1) {

                if ((System.currentTimeMillis() - tiempoDescarga) > 500) {
                    float tiempoTranscurrido = ((float) (System.currentTimeMillis() - tiempoDescarga)) / 1000;
                    float velocidad = longitudAcumulada / tiempoTranscurrido;
                    firePropertyChange("velocidad", 0, velocidad);
                    tiempoDescarga = System.currentTimeMillis();
                    longitudAcumulada = 0;
                }
                longitudAcumulada = longitudAcumulada + longitud;
                fos.write(bytes, 0, longitud);
                progresoDescarga = progresoDescarga + longitud;
                setProgress((int) (progresoDescarga * 100 / tamanoFichero));
                firePropertyChange("descarga", 0, progresoDescarga);
                firePropertyChange("porcentaje", 0, (progresoDescarga * 100) / tamanoFichero);
                if(isCancelled()){
                    cancelada = false;
                    break;
                }

            }
            is.close();
            fos.close();
            if(!cancelada){
                setProgress((int) (progresoDescarga * 100 / tamanoFichero));
                firePropertyChange("descarga_cancelada",false,true);
            }else{
                setProgress(100);
            }


        }
        return null;
    }
}
